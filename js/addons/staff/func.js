(function(_, $) {
    $(document).ready(function () {
        $(".staff_meber_email_link").on('click', staff_get_member_email);
    });

    function staff_get_member_email()
    {
        var member_id = $(this).data('member-id');
        var result_id = 'staff_member_a'+member_id;

         $.ceAjax('request', fn_url("staff_client.get_email"), {
            data: {
                member_id: member_id
            },
            method: 'GET',
            callback: function (data)
            {
                if (data.text != undefined) {

                    if (data.text!='') {
                        $("#staff_member_"+member_id+"_email_link").attr("src", "mailto:"+data.text);
                        $("#staff_member_"+member_id+"_email_link").html(data.text);
                        $("#staff_member_"+member_id).hide();
                        $("#staff_member_"+member_id+"_email_link").show();
                        return;
                    }
                }
                $("#staff_member_"+member_id).hide();
                $("#staff_member_"+member_id+"_no_email").show();
            },
            cache: false
        });

        //alert(1);
        return false;
    }
})(Tygh, Tygh.$);