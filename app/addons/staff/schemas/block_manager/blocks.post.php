<?php

$schema['staff_members'] = array (
    'content' => array (
        'items' => array (
            'remove_indent' => true,
            'hide_label' => true,
            'type' => 'enum',
            'object' => 'staff_members',
            'items_function' => 'fn_staff_get_members',
            'fillings' => array (
                'manually' => array (
                    'picker' => 'addons/staff/pickers/staff_members/picker.tpl',
                    'picker_params' => array (
                        'type' => 'links',
                        'positions' => true
                    )
                )
            ),
        ),
    ),
    'templates' => array (
        'addons/staff/blocks/original.tpl' => array()
    ),
    'wrappers' => 'blocks/wrappers',
    'cache' => array(
        'update_handlers' => array(
            'staff', 'staff_photos'
        )
    )
);

return $schema;
