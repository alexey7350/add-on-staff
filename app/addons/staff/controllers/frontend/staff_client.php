<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']  == 'POST') {
    
    if ($mode == 'get_email') {
        if (defined('AJAX_REQUEST')) {
            
            $member_emails = fn_staff_get_member_email($_REQUEST['member_id']);
            if (!empty($member_emails))
            {
                $emails_variants = explode("|", $member_emails);
                $email = $emails_variants[0]?$emails_variants[0]:$emails_variants[1];
            }

            echo $email;
            exit();
        }
    }
}

if ($mode == 'get_email') {

    if (defined('AJAX_REQUEST')) {
        
        $member_emails = fn_staff_get_member_email($_REQUEST['member_id']);
        if (!empty($member_emails)) {
            $emails_variants = explode("|", $member_emails);
            $email = $emails_variants[0]?$emails_variants[0]:$emails_variants[1];
        } else {
            $email = '';
        }
        
        echo $email;
        exit();
    }
}