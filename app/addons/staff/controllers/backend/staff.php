<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']	== 'POST') {
	
    if ($mode == 'delete') {
        fn_staff_delete_member_by_id($_REQUEST['member_id']);
        $suffix = ".manage";
    }

	if ($mode == 'update') {
		$member_id = fn_staff_update_member($_REQUEST['staff_data'], $_REQUEST['member_id']);
		$suffix = ".manage";
	}

	return array(CONTROLLER_STATUS_OK, 'staff' . $suffix);
}

if ($mode == 'manage' || $mode == 'picker') {


    $staff_members = db_get_array("SELECT ?:staff.*, ?:users.lastname, ?:users.email as useremail FROM ?:staff LEFT JOIN ?:users ON ?:staff.user_id = ?:users.user_id  WHERE 1", '');

    foreach($staff_members as $key=>$staff_member)
    {
        $staff_members[$key]['last_name'] = !empty($staff_members[$key]['last_name'])?$staff_members[$key]['last_name']:$staff_members[$key]['lastname'];
        $staff_members[$key]['email'] = !empty($staff_members[$key]['email'])?$staff_members[$key]['email']:$staff_members[$key]['useremail'];
    }

    Tygh::$app['view']->assign('staff_members', $staff_members);

} elseif ($mode == 'update'){

	$member = fn_staff_get_staff_member_data($_REQUEST['member_id']);

	if (empty($member))
	{
        return array(CONTROLLER_STATUS_NO_PAGE);
	}

	Tygh::$app['view']->assign('member', $member);

} 
if ($mode == 'picker')
{
    Tygh::$app['view']->display('addons/staff/pickers/staff_members/picker_contents.tpl');
    exit;
}

if ($mode == 'test')
{
    //fn_print_r(fn_staff_get_name(1));
    echo fn_staff_get_name(6);
}

