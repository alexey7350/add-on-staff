<?php

use Tygh\BlockManager\Block;

function fn_staff_get_member_email($member_id)
{
    if (!empty($member_id)) {
        $emails = db_get_row("SELECT ?:staff.email, ?:users.email as useremail FROM ?:staff LEFT JOIN ?:users ON ?:staff.user_id = ?:users.user_id WHERE ?:staff.member_id = ?i", $member_id);
        return $emails['email']."|".$emails['useremail'];
    }

    return false;
}


function fn_staff_get_name($member_id)
{   
    if (!empty($member_id)) {
        $combined_name = db_get_row("SELECT ?:staff.first_name, ?:staff.last_name, ?:users.lastname FROM ?:staff LEFT JOIN ?:users ON ?:staff.user_id = ?:users.user_id WHERE member_id = ?i", $member_id);

        if (!empty($combined_name)) {
            $member_name = $combined_name['first_name']." ".(!empty($combined_name['last_name'])?$combined_name['last_name']:$combined_name['lastname']);
            return $member_name;
        } else {
            return '';
        }
        
    }

    return false;
}

function fn_staff_get_members($params=array())
{   

    $default_params = array(
        'items_per_page' => 0,
    );

    $params = array_merge($default_params, $params);

    $condition = $limit = '';

    if (!empty($params['limit'])) {
        $limit = db_quote(' LIMIT 0, ?i', $params['limit']);
    }

    if (!empty($params['item_ids'])) {
        if (!is_array($params['item_ids'])) {
            $condition .= db_quote(' AND ?:staff.member_id IN (?n)', explode(',', $params['item_ids']));
        } else {
            $condition .= db_quote(' AND ?:staff.member_id IN (?n)', $params['item_ids']);
        }
    }

    $fields = array (
        '?:staff.*',
        '?:users.firstname',
        '?:users.lastname',
        '?:users.email AS useremail',
        '?:staff_photos.member_photo_id'
    );
    
    $staff_members = db_get_hash_array("SELECT ?p FROM ?:staff LEFT JOIN ?:staff_photos ON ?:staff.member_id = ?:staff_photos.member_id LEFT JOIN ?:users ON ?:staff.user_id = ?:users.user_id WHERE 1 ?p ?p", 'member_id', implode(", ", $fields), $condition, $limit);

    $member_photo_ids = fn_array_column($staff_members, 'member_photo_id');

    $photos = fn_get_image_pairs($member_photo_ids, 'photo',  'M', true, false);

    foreach($staff_members as $member_id=>$member) {
        if (!empty($photos[$member['member_photo_id']])) {
            $staff_members[$member_id]['photo_pair'] = reset($photos[$member['member_photo_id']]);

        } else {
            $staff_members[$member_id]['photo_pair'] = array();
        }
        
        $staff_members[$member_id]['last_name']=!empty($staff_members[$member_id]['last_name'])?$staff_members[$member_id]['last_name']:$staff_members[$member_id]['lastname'];
        $staff_members[$member_id]['email']=!empty($staff_members[$member_id]['email'])?$staff_members[$member_id]['email']:$staff_members[$member_id]['useremail'];
    } 

    return array($staff_members, $params);
    //return $staff_members;
}


function fn_staff_remove_all_photos()
{
    $member_photo_ids = db_get_fields("SELECT ?:staff_photos.member_photo_id FROM ?:staff_photos WHERE 1");
    foreach($member_photo_ids as $member_photo_id)
    {
        fn_delete_image_pairs($member_photo_id, 'photo');
    }
}

function fn_staff_check_user_id($user_id)
{
    return db_get_field("SELECT user_id FROM ?:users WHERE user_id = ?i", $user_id);
}

function fn_staff_delete_member_by_id($member_id)
{
    db_query("DELETE FROM ?:staff WHERE member_id = ?i", $member_id);

    Block::instance()->removeDynamicObjectData('staff_members', $member_id);

    $member_photo_ids = db_get_fields("SELECT member_photo_id FROM ?:staff_photos WHERE member_id = ?i", $member_id);
    foreach($member_photo_ids as $member_photo_id) {
        fn_delete_image_pairs($member_photo_id, 'photo');
    }
    db_query("DELETE FROM ?:staff_photos WHERE member_id = ?i", $member_id);
}


function fn_staff_get_staff_member_data($member_id)
{
	$fields = $joins = array();
    $condition = '';

    $fields = array (
        '?:staff.*',
        '?:staff_photos.member_photo_id'
    );

    $joins[] = db_quote("LEFT JOiN ?:staff_photos ON ?:staff.member_id=?:staff_photos.member_id");

	$condition.=db_quote('WHERE ?:staff.member_id = ?i', $member_id);
	$member = db_get_row("SELECT ".implode(",", $fields)." FROM ?:staff ".implode(",", $joins)." ".$condition);

	if (!empty($member))
	{
		$member['photo_pair']=fn_get_image_pairs($member['member_photo_id'], 'photo', 'M', true, false);
	}

	return $member;
}



function fn_staff_update_member($data, $member_id)
{

    if (!empty($data['user_id'])) {
        $user_id_exists = fn_staff_check_user_id($data['user_id']);
        if (empty($user_id_exists)){
            fn_set_notification('W', __('warning'), __('staff.user_not_found'));
            $data['user_id']='0';
        }
    }

	if (empty($member_id)) {

		$member_id = db_query("REPLACE INTO ?:staff ?e", $data);
		if (fn_staff_need_image_update()) {
			$member_photo_id = db_get_next_auto_increment_id('staff_photos');
			$pair_data = fn_attach_image_pairs('staff_photo', 'photo', $member_photo_id);

			if (!empty($pair_data)) {
				$data_member_photo = array(
                    'member_photo_id' => $member_photo_id,
                    'member_id'       => $member_id
                );

                db_query("INSERT INTO ?:staff_photos ?e", $data_member_photo);
			}
		}
	} else {

        db_query("UPDATE ?:staff SET ?u WHERE member_id = ?i", $data, $member_id);

        $member_photo_id = fn_staff_get_member_photo_id($member_id);
        $member_photo_exist = !empty($member_photo_id);
        $photo_is_update = fn_staff_need_image_update();

        if ($member_photo_exist && $photo_is_update) {
            fn_delete_image_pairs($member_photo_id, 'photo');
            db_query("DELETE FROM ?:staff_photos WHERE member_id = ?i", $member_id);
            $member_photo_exist = false;
        }

        if ($photo_is_update && !$member_photo_exist) {
            $member_photo_id = db_query("INSERT INTO ?:staff_photos (member_id) VALUE(?i)", $member_id);
        }

        $pair_data = fn_attach_image_pairs('staff_photo', 'photo', $member_photo_id);
	}


	return $member_id;
}

function fn_staff_get_member_photo_id($member_id)
{
    return db_get_field("SELECT member_photo_id FROM  ?:staff_photos WHERE member_id = ?i", $member_id);
}


function fn_staff_need_image_update()
{
	if (!empty($_REQUEST['file_staff_photo_image_icon']) && array($_REQUEST['file_staff_photo_image_icon'])) {
        $image_member = reset ($_REQUEST['file_staff_photo_image_icon']);

        if ($image_member == 'staff_photo') {
            return false;
        }
    }

    return true;
}