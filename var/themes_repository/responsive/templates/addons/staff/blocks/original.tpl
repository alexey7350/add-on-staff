{script src="js/addons/staff/func.js"}

{$obj_prefix = "`$block.block_id`000"}

<div id="scroll_list_{$block.block_id}" class="owl-carousel">
    {foreach from=$items item="member" name="for_member"}
        <div class="staff_member">
        {if $member.photo_pair}
            {$image_thumb=$member.photo_pair.icon.relative_path|fn_generate_thumbnail:179:200:true}
            <img class="staff_photo" src="{$image_thumb}"  style="max-width:179px;max-height:200px" />
        {else}
            <span class="ty-no-image staff_photo" style="margin-left:30px;width: 179px; height: 200px;"><i class="ty-no-image__icon ty-icon-image" title="{__("no_image")}"></i></span>
        {/if}
        <h4>{$member.first_name}<br />{$member.last_name}</h4>
        <span class="staff_member_description">
            {$member.function}<br />
            <span id="staff_member_a{$member.member_id}">
                <a id="staff_member_{$member.member_id}" data-member-id='{$member.member_id}' class="staff_meber_email_link staff_link" href="#">{__('staff.show_email')}</a>
                <a id="staff_member_{$member.member_id}_email_link" href="#" class="staff_hide_link"></a>
                <span id="staff_member_{$member.member_id}_no_email" class="staff_hide_link staff_no_email">{__('staff.no_email')}</span>

            <!--staff_member_a{$member.member_id}--></span>
        </span>
    </div>    

    {/foreach}
</div>


{script src="js/lib/owlcarousel/owl.carousel.min.js"}
<script type="text/javascript">
(function(_, $) {

    $.ceEvent('on', 'ce.commoninit', function(context) {
        var elm = context.find('#scroll_list_{$block.block_id}');

        $('.ty-float-left:contains(.ty-scroller-list),.ty-float-right:contains(.ty-scroller-list)').css('width', '100%');

        var item = {$block.properties.item_quantity|default:5},
            // default setting of carousel
            itemsDesktop = 4,
            itemsDesktopSmall = 3;
            itemsTablet = 2;

        if (item > 3) {
            itemsDesktop = item;
            itemsDesktopSmall = item - 1;
            itemsTablet = item - 2;
        } else if (item == 1) {
            itemsDesktop = itemsDesktopSmall = itemsTablet = 1;
        } else {
            itemsDesktop = item;
            itemsDesktopSmall = itemsTablet = item - 1;
        }

        var desktop = [1199, itemsDesktop],
            desktopSmall = [979, itemsDesktopSmall],
            tablet = [768, itemsTablet],
            mobile = [479, 1];

        {if $block.properties.outside_navigation == "Y"}
        function outsideNav () {
            if(this.options.items >= this.itemsAmount){
                $("#owl_outside_nav_{$block.block_id}").hide();
            } else {
                $("#owl_outside_nav_{$block.block_id}").show();
            }
        }
        {/if}

        if (elm.length) {
            elm.owlCarousel({
                direction: '{$language_direction}',
                items: item,
                itemsDesktop: desktop,
                itemsDesktopSmall: desktopSmall,
                itemsTablet: tablet,
                itemsMobile: mobile,
                scrollPerPage: true,
                autoPlay: false,
                lazyLoad: true,
                slideSpeed: 400,
                stopOnHover: true,
                navigation: true,
                navigationText: ['{__("prev_page")}', '{__("next")}'],
                pagination: false
            
            });
            

        }
    });
}(Tygh, Tygh.$));
</script>
