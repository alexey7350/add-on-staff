{if $member_id == "0"}
    {assign var="member" value=$default_name}
{else}
    {assign var="member" value=$member_id|fn_staff_get_name|default:"`$ldelim`member`$rdelim`"}
{/if}

<tr {if !$clone}id="{$holder}_{$member_id}" {/if}class="cm-js-item{if $clone} cm-clone hidden{/if}">
    {if $position_field}
        <td>
            <input type="text" name="{$input_name}[{$member_id}]" value="{math equation="a*b" a=$position b=10}" size="3" class="input-micro" {if $clone}disabled="disabled"{/if} />
        </td>
    {/if}
    <td><a href="{"staff.update?member_id=`$member_id`"|fn_url}">{$member}</a></td>
    <td>
        {capture name="tools_list"}
            {if !$hide_delete_button && !$view_only}
                <li><a onclick="Tygh.$.cePicker('delete_js_item', '{$holder}', '{$member_id}', 'b'); return false;">{__("delete")}</a></li>
            {/if}
        {/capture}
        <div class="hidden-tools">
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
</tr>