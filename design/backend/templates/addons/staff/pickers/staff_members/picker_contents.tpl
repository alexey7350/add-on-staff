{if !$smarty.request.extra}
<script type="text/javascript">
(function(_, $) {
    _.tr('text_items_added', '{__("text_items_added")|escape:"javascript"}');

    $.ceEvent('on', 'ce.formpost_members_form', function(frm, elm) {

        var members = {};

        if ($('input.cm-item:checked', frm).length > 0) {
            $('input.cm-item:checked', frm).each( function() {
                var id = $(this).val();
                members[id] = $('#staff_members_' + id).text();
            });

            {literal}
            
            $.cePicker('add_js_item', frm.data('caResultId'), members, 'b', {
                '{member_id}': '%id',
                '{member}': '%item'
            });
            
            {/literal}

            $.ceNotification('show', {
                type: 'N', 
                title: _.tr('notice'), 
                message: _.tr('text_items_added'), 
                message_state: 'I'
            });
        }

        return false;
    });

}(Tygh, Tygh.$));
</script>
{/if}
</head>
<form action="{$smarty.request.extra|fn_url}" data-ca-result-id="{$smarty.request.data_id}" method="post" name="members_form">
{if $staff_members}
<table width="100%" class="table table-middle">
<thead>
<tr>
    <th>
        {include file="common/check_items.tpl"}</th>
    <th>{__("staff")}</th>
</tr>
</thead>
{foreach from=$staff_members item=member}
<tr>
    <td>
        <input type="checkbox" name="{$smarty.request.checkbox_name|default:"members_ids"}[]" value="{$member.member_id}" class="cm-item" /></td>
    <td id="staff_members_{$member.member_id}" width="100%">{$member.first_name} {$member.last_name}</td>
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{if $staff_members}
<div class="buttons-container">
    {include file="buttons/add_close.tpl" but_text=__("staff.add_members") but_close_text=__("staff.add_members_and_close") is_js=$smarty.request.extra|fn_is_empty}
</div>
{/if}

</form>
