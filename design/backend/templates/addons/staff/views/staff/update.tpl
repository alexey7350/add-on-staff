{if $member}
    {assign var="id" value=$member.member_id}
{else}
    {assign var="id" value=0}
{/if}


{capture name="mainbox"}

<form action="{""|fn_url}" method="post" class="form-horizontal form-edit" name="staff_form" enctype="multipart/form-data">
<input type="hidden" class="cm-no-hide-input" name="fake" value="1" />
<input type="hidden" class="cm-no-hide-input" name="member_id" value="{$id}" />


<div id="content_general">
	<div class="control-group">
        <label for= "elm_staff_first_name" class="control-label cm-required">{__("first_name")}</label>
        <div class="controls">
        <input type="text" name="staff_data[first_name]" id="elm_staff_first_name" value="{$member.first_name}" size="25" class="input-large" />
        </div>
    </div>
    <div class="control-group">
        <label for= "elm_staff_last_name" class="control-label">{__("last_name")}</label>
        <div class="controls">
        <input type="text" name="staff_data[last_name]" id="elm_staff_last_name" value="{$member.last_name}" size="25" class="input-large" />
        </div>
    </div>
    <div class="control-group">
        <label for= "elm_staff_email" class="control-label cm-email">{__("email")}</label>
        <div class="controls">
        <input type="text" name="staff_data[email]" id="elm_staff_email" value="{$member.email}" size="25" class="input-large" />
        </div>
    </div>
    <div class="control-group">
        <label for= "elm_staff_function" class="control-label">{__("staff.function")}</label>
        <div class="controls">
         <textarea id="elm_staff_function" name="staff_data[function]" cols="35" rows="8" class="cm-wysiwyg input-large">{$member.function}</textarea>
        </div>
    </div>

    <div class="control-group" id="banner_graphic">
        <label class="control-label">{__("image")}</label>
        <div class="controls">
            {include file="common/attach_images.tpl" image_name="staff_photo" image_object_type="promo" image_pair=$member.photo_pair image_object_id=$id no_detailed=true hide_titles=true hide_alt=true}
        </div>
    </div>
    <div class="control-group">
        <label for= "elm_staff_user_id" class="control-label cm-integer">{__("user_id")}</label>
        <div class="controls">
        <input type="text" name="staff_data[user_id]" id="elm_staff_user_id" value="{$member.user_id}" size="25" class="input-small" />
        </div>
    </div>
</div>



{capture name="buttons"}
	{if !$id}
        {include file="buttons/save.tpl" but_role="submit-link" but_target_form="staff_form" but_name="dispatch[staff.update]"}
    {else}
    	{include file="buttons/save.tpl" but_name="dispatch[staff.update]" but_role="submit-link" but_target_form="staff_form" hide_first_button=$hide_first_button hide_second_button=$hide_second_button save=$id}
    {/if}
{/capture}

</form>

{/capture}

{if !$id}
    {assign var="title" value=__("staff.add_member")}
{else}
    {assign var="title" value="{__("staff.edit_member")}: `$member.first_name` `$member.last_name`"}
{/if}

{include file="common/mainbox.tpl" title=$title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons select_languages=false}