
{capture name="mainbox"}

{if $staff_members}
<table class="table table-middle">
<thead>
<tr>
    <th>{__("name")}</th>
    <th>{__("staff.function")}</th>
    <th>{__("email")}</th>
    <th>{__("user_id")}</th>
    <th width="6%">&nbsp;</th>
</tr>
</thead>
{foreach from=$staff_members item=member}
<tr class="cm-row-status-a">
    <td>
        <a class="row-statusa-a" href="{"staff.update?member_id=`$member.member_id`"|fn_url}">{$member.first_name} {$member.last_name}</a>
    </td>
    <td>
        {$member.function}
    </td>
    <td>
        {$member.email}
    </td>
    <td>
        {if $member.user_id!=0}
            <a class="row-statusa-a" href="{"profiles.update&user_id=`$member.user_id`"|fn_url}">{__('edit')}</a>
        {/if}
    </td>
    <td>
        {capture name="tools_list"}
            <li>{btn type="list" text=__("edit") href="staff.update?member_id=`$member.member_id`"}</li>
            <li>{btn type="list" class="cm-confirm" text=__("delete") href="staff.delete?member_id=`$member.member_id`" method="POST"}</li>
        {/capture}
        <div class="hidden-tools">
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{capture name="buttons"}
    {capture name="tools_list"}
        {if $banners}
            <li>{btn type="delete_selected" dispatch="dispatch[banners.m_delete]" form="banners_form"}</li>
        {/if}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}

{capture name="adv_buttons"}
    {include file="common/tools.tpl" tool_href="staff.add" prefix="top" hide_tools="true" title=__("staff.add_member") icon="icon-plus"}
{/capture}

</form>

{/capture}
{include file="common/mainbox.tpl" title=__("staff") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons select_languages=false}
